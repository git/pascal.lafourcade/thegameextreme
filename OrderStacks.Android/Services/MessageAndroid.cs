﻿using System;
using System.Collections;
using Android.App;
using Android.Views;
using Android.Widget;
using OrderStacks.Droid.Services;
using OrderStacks.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(MessageAndroid))]
namespace OrderStacks.Droid.Services
{
    public class MessageAndroid : IMessage
    {
        public IDictionary Properties => throw new NotImplementedException();

        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}

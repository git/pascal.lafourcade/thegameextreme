﻿using System.Collections.Generic;
using OrderStacks.model;
using OrderStacks.model.@event;

namespace OrderStacks.viewmodel
{
    public class PlayerVM
    {

        public Player View { get; set; }
        public string Pseudo { get; set; }
        private List<CardVM> cardList = new List<CardVM>();

        /**
         * <param name="view">Player représenté</param>
         * 
         * Constructeur
         */
        public PlayerVM(Player view)
        {
            View = view;

            Pseudo = view.Pseudo;
            view.getCardList().ForEach(card => cardList.Add(new CardVM(card)));

            View.HandCardChanged += OnHandCardChanged;
        }

        /**
         * <param name="args">Argument(s) de l'événement</param>
         * <param name="sender">Source de l'événement</param>
         * 
         * Evénement permettant à l'utilisateur de changer les cartes qu'il possède (supprimé et/ou piocher une carte).
         */
        private void OnHandCardChanged(object sender, HandCardChangedEventArgs args)
        {
            if (args.NewCard == null)
            {
                cardList.RemoveAt(args.Position);
            }
            else
            {
                cardList.Insert(args.Position, new CardVM(args.NewCard));
            }
        }

        /**
         * <param name="card">Carte piocher</param>
         * 
         * Fonction permettant de piocher une carte
         */
        public void pioche(CardVM card)
        {
            View.pioche(card.View);
        }

        /**
         * Fonction permettant de retourner les cartes possédées par l'utilisateur.
         * 
         * <returns>Liste des cartes possédé par l'utilisateur</returns>
         */
        public List<CardVM> getCardList()
        {
            return cardList;
        }
    }
}

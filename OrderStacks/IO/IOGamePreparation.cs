﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace OrderStacks.IO
{
    class IOGamePreparation
    {
        static string pathPlayers = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "NbPlayers.xml");
        static string pathGameModeValue = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "GameModeValue.xml");
        static string pathNbStacks = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "NbStacks.xml");
        static string pathNbCards = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "NbCards.xml");
        static string pathNameFirstPlayer = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "NamePlayer.xml");

        public static void SaveParamaterGamePreparationNbPlayers(int nbPlayers)
        {
            XmlSerializer ser = new  XmlSerializer(typeof(int));
            TextWriter tw = new StreamWriter(pathPlayers);
            ser.Serialize(tw, nbPlayers);
            tw.Close();
        }
        public static int LoadParameterGamePreparationNbPlayers()
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            
            try
            {
                using (FileStream fs = new FileStream(pathPlayers, FileMode.Open))
                {
                    int players = (int)xs.Deserialize(fs);                    
                    return players;
                }                
            }
            catch
            {                
                return 0;
            }
        }

        public static void SaveParameterGamePreparationGameModeValue(int mode)
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            TextWriter tw = new StreamWriter(pathGameModeValue);
            xs.Serialize(tw, mode);
            tw.Close();
        }

        public static int LoadParameterGamePreparationGameModeValue()
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            try
            {
                using (FileStream fs = new FileStream(pathGameModeValue, FileMode.Open))
                {
                    int gameMode = (int)xs.Deserialize(fs);
                    return gameMode;
                }
            }
            catch
            {

                return 0;
            }
        }

        public static void SaveParameterGamePreparationNbStacks(int nbStack)
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            TextWriter tw = new StreamWriter(pathNbStacks);
            xs.Serialize(tw, nbStack);
            tw.Close();
        }

        public static int LoadParamaterGamePreparationNbStacks()
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            try
            {
                using (FileStream fs = new FileStream(pathNbStacks, FileMode.Open))
                {
                    int nbstacks = (int)xs.Deserialize(fs);
                    return nbstacks;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static void SaveParameterGamePreparationNbCards(int nbCard)
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            TextWriter tw = new StreamWriter(pathNbCards);
            xs.Serialize(tw, nbCard);
            tw.Close();
        }

        public static int LoadParameterGamePreparationNbCards()
        {
            XmlSerializer xs = new XmlSerializer(typeof(int));
            try
            {
                using (FileStream fs = new FileStream(pathNbCards, FileMode.Open))
                {
                    int nbCards = (int)xs.Deserialize(fs);
                    return nbCards;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static void SaveParameterGamePreparationName(string namePseudo)
        {
            XmlSerializer xs = new XmlSerializer(typeof(string));
            TextWriter tw = new StreamWriter(pathNameFirstPlayer);
            xs.Serialize(tw, namePseudo);
            tw.Close();
        }

        public static string LoadNameFromGamePrepararion()
        {
            XmlSerializer xs = new XmlSerializer(typeof(string));
            try
            {
                using (FileStream fs = new FileStream (pathNameFirstPlayer, FileMode.Open))
                {
                    string namePseudo = (string)xs.Deserialize(fs);
                    return namePseudo;
                }
            }
            catch
            {
                return "";
            }
        }
    }
}

﻿using System;
using OrderStacks.Interface;
using OrderStacks.IO;
using OrderStacks.Resx;
using Xamarin.Forms;

namespace OrderStacks.view
{
    public partial class HomePage : ContentPage
    {
        /**
         * Constructeur de la page d'accueil de l'application * 
         **/
        public HomePage()
        {
            
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            LoadParameterOptionOnHomepage();

        }

        /**
         * Bouton lancer une partie qui ouvre la page GamePreparationPage * 
         **/
        private async void OpenGame(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new GamePreparationPage());
        }
        /**
         * Bouton multijoueur qui permettait d'ouvrir un menu multijoueur *
         * ps : ce bouton à était retirer car le mode n'a pas était implémenter mais le code behind accédant à la page est toujours existant *
         **/
        private void OpenMultiPlayerMode(object sender, EventArgs args)
        {
            DependencyService.Get<IMessage>().ShortAlert("Mode en cours de développement");
            //await Navigation.PushAsync(new MultiPlayerMode());            
        }
        /**
         * Bouton règles qui ouvre la page RulesGame * 
         **/
        private async void Rules_Clicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new RulesGame());
        }

        /**
         * Chargement de dernier choisit par l'utilisateur *
         * Charge le thème à chaque fois que l'utilisateur lance l'application, à travers la méthode de chargement de la classe IOOptions *
         **/
        public void LoadParameterOptionOnHomepage()
        {
            bool b = IOOptions.LoadOptionsParameter();
            if (!b)
            {
                Application.Current.Resources["BlackColor"] = Color.Black;
                Application.Current.Resources["WhiteColor"] = Color.White;
                imageButton.Source = "Elsole.png";
                
            }
            else
            {
                Application.Current.Resources["BlackColor"] = Color.White;
                Application.Current.Resources["WhiteColor"] = Color.Black;
                imageButton.Source = "LuneSombreTheme.png";
            }
        }

        /**
         * Permet de changer le thème de l'application en cliquant sur le soleil ou la lune *
         * Sauvegarde le thème à chaque fois que l'utilisateur change de thème à travers la méthode de sauuvegarde de la classe IOOptions
         **/
        private void Theme_Clicked(object sender, EventArgs e)
        {
            bool b;
            ButtonGrid.Children.Remove(imageButton);
            
            if(homepageT.BackgroundColor == Color.Black)
            {
                b = true;
                Application.Current.Resources["BlackColor"] = Color.White;
                Application.Current.Resources["WhiteColor"] = Color.Black;
                imageButton = new ImageButton()
                {
                    BackgroundColor = Color.Transparent,
                    BorderColor = (Color)Application.Current.Resources["SkyBlueColor"]
                };
                imageButton.Source = "LuneSombreTheme.png";
            }
            else
            {
                b = false;
                Application.Current.Resources["BlackColor"] = Color.Black;
                Application.Current.Resources["WhiteColor"] = Color.White;
                imageButton = new ImageButton()
                {
                    BackgroundColor = Color.Transparent,
                    BorderColor = (Color)Application.Current.Resources["SkyBlueColor"],
                    
                    
                };
                imageButton.Source = "Elsole.png";
            }
            ButtonGrid.Children.Add(imageButton, 1, 1);
            imageButton.Clicked += Theme_Clicked;            
            imageButton.HorizontalOptions = LayoutOptions.Center;
            imageButton.VerticalOptions = LayoutOptions.End;
            IOOptions.SaveOptionParameter(b);
            
            
        }

        /**
        * Ouvre un message affichant les détails de l'application en cliquant sur l'image du point d'interrogation *        
        **/
        private async void ImageButton_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert(AppResources.StrInfo,AppResources.StrAbout, AppResources.StrCloseWind );
        }
    }
}

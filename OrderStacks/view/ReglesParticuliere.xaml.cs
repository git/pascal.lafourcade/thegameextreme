﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OrderStacks.view
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReglesParticuliere : ContentPage
    {
        public ReglesParticuliere()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /**
         * Bouton maison qui permet de retourner sur la page d'accueil *
         **/
        private async void BackToHome(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomePage());
        }

        /**
         * Bouton jouer qui permet de se rendre sur la page GamePreparationPage *
         **/
        private async void ClickToPlay(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new GamePreparationPage());
        }

        /**
         * Bouton suivant qui permet de se rendre sur la troisième page des règles *
         **/
        private async void previousPage_Rules(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
    }
}
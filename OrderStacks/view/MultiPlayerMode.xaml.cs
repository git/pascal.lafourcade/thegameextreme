﻿using System;
using Xamarin.Forms;

namespace OrderStacks.view
{
    public partial class MultiPlayerMode : ContentPage
    {
        
        public MultiPlayerMode()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /**
         * Evenement qui permettait d'heberger une partie, mais n'a jamais été implémenté *
         **/
        private void Button_ClickedCo(object sender, EventArgs args)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;

namespace OrderStacks.model.@event
{
    public class PlayerChangedEventArgs : EventArgs
    {

        public string Pseudo;
        public List<Card> NewCurrentHand;

        /**
         * <param name="newCurrentHand">Nouvelle liste de carte active</param>
         * <param name="pseudo">Pseudo du nouvelle utilisateur actif</param>
         * Constructeur
         */
        public PlayerChangedEventArgs(List<Card> newCurrentHand, string pseudo)
        {
            NewCurrentHand = newCurrentHand;
            Pseudo = pseudo;
        }
    }
}

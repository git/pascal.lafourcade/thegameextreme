﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.extreme
{
    public class ExtremeJouerUneCarte : JouerUneCarte
    {
        public ExtremeJouerUneCarte(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        public override bool play(decimal valueCard, List<Card> CurrentHand, int orderedStackSelected, Player player, List<Card> CurrentCardPlayed)
        {
            throw new NotImplementedException();
        }

        protected override bool Rule(Card card, Stack<Card> stack, bool bottomUp, Player player, List<Card> CurrentCardPlayed)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.deck;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.classic
{
    public class GameModeClassic : GameMode
    {
        /**
         * <param name="deck">Deck de carte</param>
         * <param name="piles">Piles du jeu</param>
         *
         * Constructeur
         */
        public GameModeClassic(Piles piles, Deck deck) : base(piles, deck)
        {
        }

        /**
         * <param name="nbPlayer">Nombre de joueur</param>
         * <param name="players">Pseudo du/des joueur(s)</param>
         * 
         * Fonction permettant de charger les paramètres de jeu
         */
        override public void load(int nbPlayer, List<Player> players)
        {
            gameActions.Add(new PiocherClassic(Piles));
            gameActions.Add(new JouerUneCarteClassic(Piles));
            gameActions.Add(new TerminerSonTourClassic(Piles));

            defineNbMaxCard(nbPlayer);
            distribueCard(players);
        }

        /**
         * <param name="currentHand">Liste de carte du joueur actif</param>
         * <param name="player">Joueur actif</param>
         *
         * Fonction permettant au joueur actif de piocher une carte
         */
        override public void pioche(List<Card> currentHand, Player player)
        {
            Message = "";
            ((PiocherClassic)gameActions[0]).pioche(currentHand, deck, player, nbMaxCard);
            quickSort(currentHand, 0, currentHand.Count - 1);
        }

        /**
         * <param name="player">Joueur actif</param>
         * <param name="currentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="orderedStackSelected">Pile séléctionnée</param>
         * <param name="valueCard">Valeur de la carte en train d'être joué</param>
         *
         * Fonction permettant de tenter de jouer une carte
         *
         * <returns>Booléen de carte joué</returns>
         */
        override public bool playCard(decimal valueCard, List<Card> currentHand, int orderedStackSelected, Player player, List<Card> CurrentCardPlayed)
        {
            Message = "";
            if (((JouerUneCarteClassic)gameActions[1]).play(valueCard, currentHand, orderedStackSelected, player, CurrentCardPlayed))
            {
                return true;
            }
            else
            {
                if (!end)
                {
                    Message = ((JouerUneCarteClassic)gameActions[1]).ErrorMessage;
                }
                return false;
            }
        }

        /**
         * <param name="currentHand">Liste de carte du joueur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="player">Joueur actif</param>
         * 
         * Fonction permettant de lancer la fin du tour et de vérifier la fin du jeu
         *
         * <returns>Booléen de fin de jeu</returns>
         */
        override public bool endTurn(List<Card> currentHand, List<Card> CurrentCardPlayed, Player player)
        {
            Message = "";
            if (((TerminerSonTourClassic)gameActions[2]).end(currentHand, CurrentCardPlayed))
            {
                pioche(currentHand, player);
                CurrentCardPlayed.Clear();
                OnPlayerChanged(null);
                return end;
            }
            else
            {
                Message = ((TerminerSonTourClassic)gameActions[2]).ErrorMessage;
                return false;
            }
        }

        /**
         * <param name="currentHand">Liste de carte du joueur actif</param>
         *
         * Fonction permettant de vérifier si c'est la fin du jeu
         */
        override public void TestEndGame(List<Card> currentHand)
        {
            if (((TerminerSonTourClassic)gameActions[2]).Test(currentHand))
            {
                end = false;
            }
            else
            {
                OnEndGame(new EventArgs());
                end = true;
            }
        }
    }
}

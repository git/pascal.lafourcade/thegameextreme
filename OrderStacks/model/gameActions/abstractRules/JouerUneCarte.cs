﻿using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.abstractRules
{
    public abstract class JouerUneCarte : GameAction
    {

        public Card OldCard { get; set; }

        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        protected JouerUneCarte(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="player">Joueur actif</param>
         * <param name="currentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="orderedStackSelected">Pile séléctionnée</param>
         * <param name="valueCard">Valeur de la carte en train d'être joué</param>
         *
         * Fonction permettant de tenter de jouer une carte
         *
         * <returns>Booléen de carte joué</returns>
         */
        public abstract bool play(decimal valueCard, List<Card> CurrentHand, int orderedStackSelected, Player player, List<Card> CurrentCardPlayed);

        /**
         * <param name="player">Joueur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="bottomUp">Booléen d'ascendance</param>
         * <param name="card">Carte joué</param>
         * <param name="stack">Pile joué</param>
         *
         * Fonction permettant de tenter de jouer une carte
         *
         * <returns>Booléen de carte joué</returns>
         */
        protected abstract bool Rule(Card card, Stack<Card> stack, bool bottomUp, Player player, List<Card> CurrentCardPlayed);
    }
}

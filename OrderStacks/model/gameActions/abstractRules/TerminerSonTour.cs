﻿using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.abstractRules
{
    public abstract class TerminerSonTour : GameAction
    {
        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        protected TerminerSonTour(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="CurrentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         *
         * Fonction permettant de terminer son tour
         *
         * <returns>Booléen indiquant si le changement de joueur peut se passer</returns>
         */
        public abstract bool end(List<Card> CurrentHand, List<Card> CurrentCardPlayed);

        /**
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         * 
         * Fonction permettant de gérer le test de fin du jeu
         *
         * <returns>Booléen de possibilité de jeu</returns>
         */
        public bool Test(List<Card> CurrentHand)
        {
            if (CurrentHand.Count > 0 )
            {
                List<Card> playableCard = new List<Card>();
                tryToFindSoluce(playableCard, CurrentHand);
                return testEndGame(playableCard, CurrentHand);
            }
            return false;
        }

        /**
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         * <param name="playableCard">Liste des cartes jouables</param>
         *
         * Fonction permettant de chercher les cartes pouvants être jouées et les ajoutes à la liste des cartes jouables
         */
        protected abstract void tryToFindSoluce(List<Card> playableCard, List<Card> CurrentHand);

        /**
         * <param name="playableCard">Liste des cartes jouables</param>
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         *
         * Fonction vérifiant que les règles de fin de jeu ne sont pas arrivé
         *
         * <returns>Booléen de possibilité de jeu</returns>
         */
        protected abstract bool testEndGame(List<Card> playableCard, List<Card> CurrentHand);
    }
}

﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.deck;
using OrderStacks.model.@event;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.abstractRules
{
    public abstract class GameMode
    {
        protected List<GameAction> gameActions;
        public Piles Piles { get; protected set; }
        public int NbCardAtBeginOfTurn { get; set; }
        protected Deck deck;
        protected int nbMaxCard;
        protected bool end;
        public string Message { get; set; } = "";


        #region

        public event EventHandler<EventArgs> EndGame;
        public event EventHandler<PlayerChangedEventArgs> PlayerChanged;
        protected internal void OnEndGame(EventArgs args)
        {
            EndGame?.Invoke(this, args);
        }
        protected internal void OnPlayerChanged(PlayerChangedEventArgs args)
        {
            PlayerChanged?.Invoke(this, args);
        }

        #endregion

        /**
         * <param name="deck">Deck de carte</param>
         * <param name="piles">Piles du jeu</param>
         *
         * Constructeur
         */
        protected GameMode(Piles piles, Deck deck)
        {
            gameActions = new List<GameAction>();
            Piles = piles;
            this.deck = deck;
        }

        /**
         * <param name="nbPlayer">Nombre de joueur</param>
         * <param name="players">Pseudo du/des joueur(s)</param>
         * 
         * Fonction permettant de charger les paramètres de jeu
         */
        public abstract void load(int nbPlayer, List<Player> players);

        protected void defineNbMaxCard(int nbPlayer)
        {
            switch (nbPlayer)
            {
                case 1:
                    nbMaxCard = 8;
                    NbCardAtBeginOfTurn = 8;
                    break;
                case 2:
                    nbMaxCard = 7;
                    NbCardAtBeginOfTurn = 7;
                    break;
                default:
                    nbMaxCard = 6;
                    NbCardAtBeginOfTurn = 6;
                    break;
            }
        }

        /**
         * <param name="ar">Liste de carte</param>
         * <param name="start">Index de départ</param>
         * <param name="end">Index de fin</param>
         * 
         * Fonction de tri
         */
        protected void quickSort(List<Card> ar, int start, int end)
        {
            if (start < end)
            {
                Card pivot = ar[end];
                int pIndex = start;
                Card swap;

                for (int i = start; i < end; i++)
                {
                    if (ar[i].Value.CompareTo(pivot.Value) < 0)
                    {
                        swap = ar[pIndex];
                        ar[pIndex] = ar[i];
                        ar[i] = swap;
                        pIndex++;
                    }
                }

                ar[end] = ar[pIndex];
                ar[pIndex] = pivot;

                quickSort(ar, start, pIndex - 1);
                quickSort(ar, pIndex + 1, end);
            }
        }

        /**
         * <param name="players">Liste de joueur</param>
         *
         * Fonction permettant de distribuer les cartes en début de partie
         */
        protected void distribueCard(List<Player> players)
        {
            players.ForEach(player =>
            {
                for (int i = 0; i < nbMaxCard; i++)
                {
                    int r = new Random().Next(0, deck.size() - 1);
                    player.pioche(deck.getCard(r));
                    deck.removeAt(r);
                }
                List<Card> cards = player.getCardList();
                quickSort(cards, 0, cards.Count - 1);
            });
        }

        /**
         * <param name="currentHand">Liste de carte du joueur actif</param>
         * <param name="player">Joueur actif</param>
         *
         * Fonction permettant au joueur actif de piocher une carte
         */
        public abstract void pioche(List<Card> currentHand, Player player);

        /**
         * <param name="player">Joueur actif</param>
         * <param name="currentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="orderedStackSelected">Pile séléctionnée</param>
         * <param name="valueCard">Valeur de la carte en train d'être joué</param>
         *
         * Fonction permettant de tenter de jouer une carte
         *
         * <returns>Booléen de carte joué</returns>
         */
        public abstract bool playCard(decimal valueCard, List<Card> currentHand, int orderedStackSelected, Player player, List<Card> CurrentCardPlayed);

        /**
         * <param name="currentHand">Liste de carte du joueur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="player">Joueur actif</param>
         * 
         * Fonction permettant de lancer la fin du tour et de vérifier la fin du jeu
         *
         * <returns>Booléen de fin de jeu</returns>
         */
        public abstract bool endTurn(List<Card> currentHand, List<Card> CurrentCardPlayed, Player player);

        /**
         * <param name="currentHand">Liste de carte du joueur actif</param>
         *
         * Fonction permettant de vérifier si c'est la fin du jeu
         */
        public abstract void TestEndGame(List<Card> currentHand);

        /**
         * Fonction permettant de retourner le nombre de carte du deck
         *
         * <returns>Nombre de carte dans le deck</returns>
         */
        public int getScore()
        {
            return deck.size();
        }
    }
}

﻿using System.Collections.Generic;
using OrderStacks.model.gameActions.abstractRules;

namespace OrderStacks.model
{
    public class Parametreur
    {

        public GameMode GameMode { get; set; }
        public List<Player> players = new List<Player>();
        public int NbPlayer { get; set; }

        /**
         * <param name="gameMode">Mode de jeu utilisé</param>
         * 
         * Constructeur
         */
        public Parametreur(GameMode gameMode)
        {
            GameMode = gameMode;
        }

        /**
         * Fonction permettant de préparer le jeu
         */
        public void Prepare()
        {
            NbPlayer = players.Count;
            GameMode.load(NbPlayer, players);
        }

        /**
         *<param name="player">Joueur à ajouter</param>
         * 
         * Fonction permettant d'ajouter un joueur
         */
        public void AddPlayer(Player player)
        {
            if (player != null)
            {
                players.Add(player);
            }
        }

        /**
         * Fonction permettant de retourner le nombre de cartes restantes à jouer
         *
         * <returns>Nombre de carte restante à jouer</returns>
         */
        public string getScore()
        {
            int score = GameMode.getScore();
            players.ForEach(player =>
            {
                score += player.getCardList().Count;
            });
            return score.ToString();
        }
    }
}

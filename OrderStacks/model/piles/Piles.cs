﻿using System.Collections.Generic;
using OrderStacks.model.card;

namespace OrderStacks.model.piles
{
    public abstract class Piles
    {

        protected List<Stack<Card>> ListOrderedStacks { get; set; } = new List<Stack<Card>>();
        public int Size { get; set; }

        /**
         * <param name="nbPile">Nombre de pile</param>
         *
         * Constructeur
         */
        public Piles(int nbPile)
        {
            Size = nbPile;
            for (int i = 0; i < nbPile; i++)
            {
                ListOrderedStacks.Add(new Stack<Card>());
            }
        }

        /**
         * <param name="i">Index de la pile</param>
         *
         * Fonction permettant de retourner une pile grâce à son index
         *
         * <returns>Pile de carte</returns>
         */
        public Stack<Card> getStack(int i)
        {
            return ListOrderedStacks[i];
        }
    }
}

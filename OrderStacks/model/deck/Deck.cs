﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;

namespace OrderStacks.model.deck
{
    public abstract class Deck
    {
        protected List<Card> deck = new List<Card>();
        protected int nbCard;

        /**
         * <param name="nbCard">Nombre de carte dans le deck</param>
         * Constructeur
         */
        protected Deck(int nbCard)
        {
            this.nbCard = nbCard;
        }

        /**
         * Fonction retournant le nombre de carte dans le deck
         */
        public int size()
        {
            return deck.Count;
        }

        /**
         * <param name="index">Index supprimé</param>
         * Fonction permettant de retirer un index du deck
         */
        public void removeAt(int index)
        {
            deck.RemoveAt(index);
        }

        /**
         * <param name="index">Index de la carte chercher</param>
         * Fonction retournant la carte contenu dans l'index
         * <returns>Carte à la position chercher</returns>
         */
        public Card getCard(int index)
        {
            return deck[index];
        }

        /**
         * <param name="card">Carte que l'on souhaite insérer</param>
         * <param name="start">Début de la plage de recherche</param>
         * <param name="end">Fin de la plage de recherche</param>
         * Fonction permet d'insérer dichotomiquement une carte
         * N'insére pas de carte en double
         */
        protected void InsertionDichotomique(int start, int end, Card card)
        {
            if (deck.Count == 0)
            {
                deck.Add(card);
                return;
            }

            int mediane = (end - start) / 2 + start;
            int comparateur = deck[mediane].Value.CompareTo(card.Value);

            if (comparateur == 0)
            {
                return;
            }

            if (mediane == start)
            {
                if (comparateur < 0)
                {
                    if (deck[end].Value.CompareTo(card.Value) < 0)
                    {
                        deck.Insert(end + 1, card);
                        return;
                    }
                    else if (deck[end].Value.CompareTo(card.Value) > 0)
                    {
                        deck.Insert(end, card);
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    deck.Insert(start, card);
                    return;
                }
            }
            else
            {
                if (comparateur < 0)
                {
                    InsertionDichotomique(mediane, end, card);
                    return;
                }
                else
                {
                    InsertionDichotomique(start, mediane, card);
                    return;
                }
            }
        }
    }
}

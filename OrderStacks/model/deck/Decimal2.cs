﻿using System;
using OrderStacks.model.card.cardType;

namespace OrderStacks.model.deck
{
    public class Decimal2Deck : Deck
    {
        /**
         * <param name="borneMin">Valeur minimale du deck</param>
         * <param name="borneMax">Valeur maximale du deck</param>
         * <param name="nbCard">Nombre de carte dans le deck</param>
         * Constructeur
         */
        public Decimal2Deck(int nbCard, int borneMin, int borneMax) : base(nbCard)
        {
            Random random = new Random();
            while (deck.Count < nbCard && deck.Count < (borneMax - borneMin))
            {
                decimal value = random.Next(borneMin, borneMax) * 0.01m;
                InsertionDichotomique(0, deck.Count - 1, new ClassicCard(value));
            }
        }
    }
}

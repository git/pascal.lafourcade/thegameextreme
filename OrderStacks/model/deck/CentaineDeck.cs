﻿using System;
using OrderStacks.model.card.cardType;

namespace OrderStacks.model.deck
{
    public class CentaineDeck : Deck
    {
        /**
         * <param name="borneMin">Valeur minimale du deck</param>
         * <param name="borneMax">Valeur maximale du deck</param>
         * <param name="nbCard">Nombre de carte dans le deck</param>
         * Constructeur
         */
        public CentaineDeck(int nbCard, decimal borneMin, decimal borneMax) : base(nbCard)
        {
            Random random = new Random();
            int borneMinRandom = (int)(borneMin * 100);
            int borneMaxRandom = (int)(borneMax * 100);
            while (deck.Count < nbCard && deck.Count < (borneMaxRandom - borneMinRandom))
            {
                int value = random.Next(borneMinRandom, borneMaxRandom);
                InsertionDichotomique(0, deck.Count-1, new FractionCard(new Fraction(value, 100, 3)));
            }
        }
    }
}

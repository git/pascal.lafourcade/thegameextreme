﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.card.cardType;
using OrderStacks.model.card.rapidCard;

namespace OrderStacks.model.deck
{
    public class ExtremeDeck : Deck
    {

        private List<int> endGame;
        private List<int> threeCard;
        private Random random = new Random();

        public ExtremeDeck(int nbCard) : base(nbCard)
        {
            endGame = new List<int>();
            threeCard = new List<int>();

            createEndCard();
            createThreeCard();

            for (int i = 2; i <= 99; i++)
            {
                deck.Add(createCard(i));
            }
        }

        private void createEndCard()
        {
            while (endGame.Count < 4)
            {
                int r = random.Next(2, 99);
                if (!endGame.Contains(r))
                {
                    endGame.Add(r);
                }
            }
        }

        private void createThreeCard()
        {
            while (threeCard.Count < 4)
            {
                int r = random.Next(2, 99);
                if (!endGame.Contains(r) && !threeCard.Contains(r))
                {
                    threeCard.Add(r);
                }
            }
        }

        private Card createCard(int iteration)
        {
            if (endGame.Contains(iteration))
            {
                return new EndGameCard(iteration);
            }
            else if (threeCard.Contains(iteration))
            {
                return new ThreeCard(iteration);
            }
            else
            {
                return new ClassicCard(iteration);
            }
        }
    }
}

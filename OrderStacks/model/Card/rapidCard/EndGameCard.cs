﻿using System;

namespace OrderStacks.model.card.rapidCard
{
    public class EndGameCard : RapidCard
    {
        /**
         * Type de carte
         */
        public static readonly string CARD_ENDGAME = "EndGameCard";

        /**
         * <param name="value">Valeur de la carte</param>
         * Constructeur
         */
        public EndGameCard(int value)
            :base(value)
        {
        }

        override public string getName()
        {
            return CARD_ENDGAME;
        }

    }
}

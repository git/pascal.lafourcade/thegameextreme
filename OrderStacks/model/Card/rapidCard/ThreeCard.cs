﻿using System;

namespace OrderStacks.model.card.rapidCard
{
    public class ThreeCard : RapidCard
    {
        /**
         * Type de carte
         */
        public static readonly string CARD_THREE = "ThreeCard";

        /**
         * <param name="value">Valeur de la carte</param>
         * Constructeur
         */
        public ThreeCard(int value)
            :base(value)
        {
        }

        override public string getName()
        {
            return CARD_THREE;
        }

    }
}

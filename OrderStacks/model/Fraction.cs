﻿using System;

namespace OrderStacks.model
{
    public class Fraction
    {
        public int Numerateur;
        public int Denominateur;
        public int SizeMax;

        /**
         * <param name="numerateur">Numérateur de la fraction</param>
         * <param name="denominateur">Dénominateur de la fraction</param>
         * <param name="sizeMax">Nombre maximal de chiffre présent dans la fraction</param>
         *
         * Constructeur
         */
        public Fraction(int numerateur, int denominateur, int sizeMax)
        {
            Numerateur = numerateur;
            Denominateur = denominateur;
            SizeMax = sizeMax;
        }

        /**
         * <param name="fraction">Fraction testé</param>
         *
         * Fonction de permettant de regarder si deux fractions sont multiple sans posséder le même chiffre ou un numérateur/dénominateur égale à 1
         */
        public bool isMultiple(Fraction fraction)
        {
            bool numIsZero = false;
            bool denIsZero = false;
            if (fraction.Numerateur == 0)
            {
                numIsZero = true;
            }
            if (fraction.Denominateur == 0)
            {
                denIsZero = true;
            }
            if (fraction.Numerateur == 1 && denIsZero)
            {
                if (fraction.Denominateur != 1
                    && fraction.Denominateur != Denominateur
                    && Denominateur % fraction.Denominateur == 0)
                {
                    return true;
                }
            }
            else if (fraction.Denominateur == 1 && !numIsZero)
            {
                if (fraction.Numerateur != 1
                    && fraction.Numerateur != Numerateur
                    && Numerateur % fraction.Numerateur == 0)
                {
                    return true;
                }
            }
            else if (fraction.Numerateur == Numerateur && !denIsZero)
            {
                if (fraction.Denominateur != Denominateur
                    && Denominateur % fraction.Denominateur == 0)
                {
                    return true;
                }
            }
            else if (fraction.Denominateur == Denominateur && !numIsZero)
            {
                if (fraction.Numerateur != Numerateur
                    && Numerateur % fraction.Numerateur == 0)
                {
                    return true;
                }
            }
            else
            {
                if (!numIsZero)
                {
                    if (Numerateur % fraction.Numerateur == 0)
                    {
                        return true;
                    }
                }
                if (denIsZero)
                {
                    if (Denominateur % fraction.Denominateur == 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * <param name="fraction">Fraction testé</param>
         *
         * Fonction permettant de savoir si deux fractions paossèdent des diviseurs commums
         *
         * <returns>Booléen de possession de diviseur commun</returns>
         */
        public bool testDiviseurCommun(Fraction fraction)
        {
            if (PGCD(Numerateur, fraction.Numerateur) == Numerateur || PGCD(Denominateur, fraction.Denominateur) == Denominateur)
            {
                return true;
            }
            return false;
        }

        /**
         * <param name="a">Numérateur</param>
         *
         * Fonction permettant de retourner le PGCD (Plus Grand Commun Diviseur) de deux fractions
         *
         * <returns>Plus Grand Commun Diviseur</returns>
         */
        private int PGCD(int a, int b)
        {
            int temp = a % b;
            if (temp == 0)
                return b;
            return PGCD(b, temp);
        }

        /**
         * Fonction permettant d'obtenir la valeur décimale de la fraction
         *
         * <returns>Valeur décimale de la fraction</returns>
         */
        public decimal Result()
        {
            decimal result = (decimal)Numerateur / (decimal)Denominateur;
            return result;
        }

        override public string ToString()
        {
            return Numerateur.ToString() + "/" + Denominateur.ToString();
        }
    }
}
